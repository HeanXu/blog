FROM node:6.11-alpine
RUN hexo server

WORKDIR /usr/src/app

COPY package.json /usr/src/app/
RUN npm install && npm install hexo-cli -g
COPY . /usr/src/app

EXPOSE 80

ENTRYPOINT hexo server
