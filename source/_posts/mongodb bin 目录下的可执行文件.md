
---
title: mongodb bin 目录下的可执行文件
---

打开你的 mongodb/bin 目录，你可以看到有以下文件：

<div style="text-align: center;"><img src="https://wx2.sinaimg.cn/mw690/e9a0fad9ly1fmxt0dqvmij21kw03ljtn.jpg" style="width:100%;"></div>

###### 他们的作用分别是：
<table>
  <tr>
    <th>bsondump</th>
    <th>查看或是调试 .bson 文件</th>
  </tr>
  <tr>
    <th>mongo</th>
    <th>连接到 MongoDB 客户端</th>
  </tr>
  <tr>
    <th>mongod</th>
    <th>启动 MongoDB</th>
  </tr>
  <tr>
    <th>mongodump</th>
    <th>备份程序</th>
  </tr>
  <tr>
    <th>mongoexport</th>
    <th>从数据库中导出 CSV 或者 JSON 格式的数据</th>
  </tr>
  <tr>
    <th>mongoimport</th>
    <th>导入 CSV，TSV 或者 JSON 格式的数据到数据库</th>
  </tr>
  <tr>
    <th>mongofiles</th>
    <th>GridFS 工具，内建的分布式文件系统</th>
  </tr>
  <tr>
    <th>mongorestore</th>
    <th>将 mongodump 备份的数据恢复到正在运行的服务中</th>
  </tr>
  <tr>
    <th>mongos</th>
    <th>数据分片程序，支持数据的横向扩展</th>
  </tr>
  <tr>
    <th>mongostat</th>
    <th>监视基本的MongoDB服务器统计信息</th>
  </tr>
</table>

#### Now, go & play with them!


